﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopItems
{
    public class Pencil : Uneatable 
    {
        public Pencil(string name, float cost)
        {
            this.Name =name;
            this.Cost = cost;
        }
        public void Drow()
        {
            Console.WriteLine("Рисует!");
        }
        public override void Show()
        {
            Console.WriteLine($"Название {Name}, стоимость {Cost}");
        }
    }
}
