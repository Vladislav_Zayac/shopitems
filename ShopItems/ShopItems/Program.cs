﻿using System;

namespace ShopItems
{
    class Program
    {
        static void Main(string[] args)
        {
            Shop shop = new Shop();
            shop.AddProduct(new Watch("Samsung",4.15f));
            shop.AddProduct(new Meet("Говядина", 1.15f));
            shop.AddProduct(new IceCream("Пломбир", 0.15f, IceCreamType.Raspberries, 7));
            shop.AddProduct(new Pencil("Ручка", 9.45f));
            shop.ShowEatable();
            shop.ShowUneatable();
            Console.WriteLine("Самый дорогой товар:");
            shop.ShowDearestItem();
            Console.WriteLine("Самый дешевый товар:");
            shop.ShowCheapest();
            shop.Execute();
        }
    }
}
