﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShopItems
{
    public class Item
    {
        protected string Name;
        protected float Cost;
        public virtual void Show()
        {

        }
        public float GetCost()
        {
            return Cost;
        }
    }
}
